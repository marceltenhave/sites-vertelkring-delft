��          �            x     y  8   }  ^  �  
           
   <     G     `     g  I   }  S   �          /  6   C  :   z  /  �  	   �  N   �  S  >     �  %   �  
   �     �  	   �     �  S     c   c     �     �  6   �  :   1	     
                        	                                                  Add Adds the ability to use categories in the media library. By default the WordPress Media Library uses the same categories as WordPress does (such as in posts &amp; pages).<br />
                    If you want to use separate categories for the WordPress Media Library take a look at our <a href="https://wordpress.org/plugins/wp-media-library-categories/#faq" target="_blank">Frequently asked questions</a>. Categories Category changes are saved. Jeffrey-WP Media Library Categories Remove Remove all categories Thank you for using the <strong>Media Library Categories</strong> plugin. Try Media Library Categories Premium today for just $20 - 100% money back guarantee Try Premium Version View all categories https://codecanyon.net/user/jeffrey-wp/?ref=jeffrey-wp https://wordpress.org/plugins/wp-media-library-categories/ PO-Revision-Date: 2016-10-19 11:53:49+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/2.4.0-alpha
Language: nl
Project-Id-Version: Plugins - Media Library Categories - Stable (latest release)
 Toevoegen Voegt de mogelijkheid toe om categorieën in de mediabibliotheek te gebruiken. Standaard gebruikt de WordPress Media Library de zelfde categorieën als WordPress (zoals in berichten &amp; pagina's).<br />
Als je eigen categorieën wilt gebruiken voor de WordPress Media Library staat dit uitgelegd in de <a href="https://wordpress.org/plugins/wp-media-library-categories/#faq" target="_blank">Veel gestelde vragen</a>. Categorieën Categoriewijzigingen zijn opgeslagen. Jeffrey-WP Media Library Categories Verwijder Verwijder alle categorieën Bedankt voor het gebruiken van de <strong>Media Library Categories</strong> plugin. Probeer Media Library Categories Premium vandaag voor maar $20 - 100% niet goed geld terug garantie Probeer de Premium versie Bekijk alle categorieën https://codecanyon.net/user/jeffrey-wp/?ref=jeffrey-wp https://wordpress.org/plugins/wp-media-library-categories/ 